/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors')

module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}'
  ],
  darkMode: 'class',
  theme: {
    container: {
      center: true,
      screens: {
        lg: '960px'
      }
    },
    extend: {
      colors: {
        primary: {
          300: '#FFCC21',
          400: '#FF963C',
          500: '#EA6C00;'
        },
        secondary: {
          300: '#8FE9D0'
        },
        dark: {
          500: '#414141',
          600: '#2E2E2E'
        },
        gray: {
          400: '#777777'
        },
        ...colors
      },
      boxShadow: {
        sm: '0px 3px 6px rgba(0, 0, 0, 0.160784)'
      }
    }
  },
  plugins: [require('@tailwindcss/line-clamp')]
}
