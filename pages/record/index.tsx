import Layout from '@/components/Layout'
import { Category, MyExercise, MyDiary, Chart } from '@/components/Pages/MyRecord'
import { Button } from '@/components/Base'

export default function Record() {

  return (
    <Layout>
      <div className="mt-[56px] mb-[64px] w-full">
        <Category />
        <Chart />
        <MyExercise />
        <MyDiary />
        <div className="text-center">
          <Button text="自分の日記をもっと見る" />
        </div>
      </div>
    </Layout>
  )
}
