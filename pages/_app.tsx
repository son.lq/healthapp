import type { AppProps } from 'next/app'
import LoginProvider from '@/context/Login'
import '../styles/globals.css'

export default function App({ Component, pageProps }: AppProps) {
  return (
    <LoginProvider>
      <Component {...pageProps} />
    </LoginProvider>
  )
}
