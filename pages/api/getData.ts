// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  try {
    const response = await fetch(`https://dummyjson.com/products?limit=10`).then((res) => res.json())
    res.status(200).json(response)
  } catch (error) {
    res.status(500).json({
      error: 'failed to load data'
    })
  }
}
