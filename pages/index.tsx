import Layout from '@/components/Layout'
import { Recommended, List } from '@/components/Pages/Home'
import { Button } from '@/components/Base'

export default function Home() {

  return (
    <Layout>
      <div className="mt-[56px] mb-[64px] w-full">
        <Recommended />
        <List />
        <div className="text-center">
          <Button />
        </div>
      </div>
    </Layout>
  )
}
