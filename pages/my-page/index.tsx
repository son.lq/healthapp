import Layout from '@/components/Layout'
import {
  ListCategory,
  List,
  Chart,
  Achievement
} from '@/components/Pages/MyPage'
import { Button } from '@/components/Base'

export default function MyPage() {
  return (
    <Layout>
      <div className="mb-[64px] w-full">
        <div className="mb-[22px] grid grid-cols-1 xl:grid-cols-10">
          <div className="xl:col-span-4">
            <Achievement />
          </div>
          <div className="xl:col-span-6">
            <Chart />
          </div>
        </div>
        <ListCategory />
        <List />
        <div className="text-center">
          <Button text="記録をもっと見る" />
        </div>
      </div>
    </Layout>
  )
}
