import React, { useState } from 'react'
import { Inter } from '@next/font/google'
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  ResponsiveContainer
} from 'recharts'
import clsx from 'clsx'
import { TIME_TYPE, CHART } from './Data'

const inter = Inter({ subsets: ['latin'] })

export default function Chart() {
  const [currentTimeType, setCurrentTimeType] = useState(TIME_TYPE[3])

  const changeTime = (time: string) => () => {
    setCurrentTimeType(time)
  }

  return (
    <div
      className="container mx-auto mb-[56px] bg-dark-500 px-6 py-4 text-white"
      id="chart_record"
    >
      <div
        className={clsx(inter.className, 'mb-2 flex items-center font-normal')}
      >
        <div className="mr-4 text-[15px]">
          BODY <br /> RECORD
        </div>
        <div className="text-[22px]">2021.05.21</div>
      </div>
      <ResponsiveContainer height={292} width="100%">
        <LineChart data={CHART[currentTimeType]}>
          <Line type="monotone" dataKey="u1" stroke="#8FE9D0" strokeWidth={3} />
          <Line type="monotone" dataKey="u2" stroke="#FFCC21" strokeWidth={3} />
          <CartesianGrid horizontal={false} stroke="#777777" />
          <XAxis
            dataKey="name"
            stroke="#fff"
            axisLine={false}
            tickLine={false}
          />
          <YAxis width={25} tickCount={0} />
        </LineChart>
      </ResponsiveContainer>
      <div className="flex">
        {TIME_TYPE.map((item) => (
          <button
            key={item}
            className={clsx(
              item === currentTimeType
                ? 'bg-primary-300 text-white'
                : 'bg-white text-primary-300',
              'mr-4 h-6 w-[56px] rounded-full text-[15px] hover:bg-primary-300 hover:text-white'
            )}
            onClick={changeTime(item)}
          >
            {item}
          </button>
        ))}
      </div>
    </div>
  )
}
