import React, { useState, useEffect } from 'react'
import { Inter } from '@next/font/google'
import clsx from 'clsx'
import Image from 'next/image'
import Link from 'next/link'
import { nanoid } from 'nanoid'
import { CATEGORY } from './Data'

const inter = Inter({ subsets: ['latin'] })

export default function Category() {
  const [loadSkeleton, setLoadSkeleton] = useState(true)
  const [data, setData] = useState([])
  const [firstCome, setFirstCome] = useState(true)

  useEffect(() => {
    if (firstCome) {
      // React 18 - Avoiding Use Effect Getting Called Twice
      setFirstCome(false)
      return
    }
    getData()
  }, [firstCome])

  const getData = () => {
    fetch('/api/getData', {})
      .then((res) => res.json())
      .then(({ products }: any) => {
        if (products?.length) {
          setData(CATEGORY)
        }
        setLoadSkeleton(false)
      })
      .catch((err) => {
        setLoadSkeleton(false)
        console.debug('err', err)
      })
  }

  return (
    <div className="container mx-auto mb-[56px]">
      <div className="grid grid-cols-1 gap-[48px] md:grid-cols-3">
        {loadSkeleton
          ? Array.from(Array(3).keys()).map((v) => (
              <div
                key={nanoid()}
                className="flex h-[288px] animate-pulse flex-col items-center justify-center space-y-4  divide-y divide-gray-200 border-gray-200 p-4 shadow dark:divide-gray-700 dark:border-gray-700 md:p-6"
              >
                <div role="status" className="w-full" key={nanoid()}>
                  <div className="mb-4 h-2.5 w-48 rounded-full bg-gray-200 dark:bg-gray-700"></div>
                  <div className="mb-2.5 h-2 max-w-[260px] rounded-full bg-gray-200 dark:bg-gray-700"></div>
                  <div className="mb-2.5 h-2 rounded-full bg-gray-200 dark:bg-gray-700"></div>
                  <div className="mb-2.5 h-2 max-w-[220px] rounded-full bg-gray-200 dark:bg-gray-700"></div>
                  <div className="mb-2.5 h-2 max-w-[200px] rounded-full bg-gray-200 dark:bg-gray-700"></div>
                  <div className="h-2 max-w-[360px] rounded-full bg-gray-200 dark:bg-gray-700"></div>
                  <span className="sr-only">Loading...</span>
                </div>
              </div>
            ))
          : data.map((item) => (
              <div
                className="relative flex flex-col items-center bg-primary-300 p-6"
                key={item.buttonLabel}
              >
                <div className="group relative">
                  <div className="aspect-square w-full">
                    <Image
                      className="h-full object-cover grayscale group-hover:grayscale-0"
                      alt={item.image}
                      priority
                      src={require(`@/assets/images/${item.image}.jpg`)}
                    />
                  </div>
                  <div className="z-1 absolute inset-0 h-full w-full bg-black bg-opacity-60 group-hover:bg-opacity-0"></div>
                  <div className="absolute inset-0 flex flex-col items-center justify-center ">
                    <div
                      className={clsx(
                        inter.className,
                        'tex mb-[10px] text-[25px] font-normal text-primary-300 md:text-[20px] lg:text-[25px]'
                      )}
                    >
                      {item.title}
                    </div>
                    <button className="h-6 w-[160px] bg-primary-400 text-sm text-white">
                      <Link href={`#${item.buttonId}`} passHref>
                        {item.buttonLabel}
                      </Link>
                    </button>
                  </div>
                </div>
              </div>
            ))}
      </div>
    </div>
  )
}
