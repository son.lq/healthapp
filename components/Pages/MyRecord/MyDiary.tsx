import React from 'react'
import { Inter } from '@next/font/google'
import clsx from 'clsx'
import { nanoid } from 'nanoid'
import { DIARY } from './Data'

const inter = Inter({ subsets: ['latin'] })

export default function MyDiary() {
  return (
    <div className="container mx-auto mb-[30px]" id="my_diary">
      <div className={clsx(inter.className, 'text-[22px] font-normal')}>
        MY DIARY1
      </div>
      <div className="grid grid-cols-1 gap-3 lg:grid-cols-4">
        {Array.from(Array(8).keys()).map(() => (
          <div className="border-2 border-[#707070] p-4" key={nanoid()}>
            <div className={clsx(inter.className, 'mb-2 text-lg font-normal')}>
              <div className="">{DIARY.day}</div>
              <div className="">{DIARY.time}</div>
            </div>
            <div className="text-xs">{DIARY.content}</div>
            <div className="text-xs">{DIARY.content1}</div>
          </div>
        ))}
      </div>
    </div>
  )
}
