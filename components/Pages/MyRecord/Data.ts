export const CATEGORY = [
  {
    title: 'BODY RECORD',
    buttonLabel: '自分のカラダの記録',
    image: 'MyRecommend-1',
    buttonId: 'chart_record'
  },
  {
    title: 'MY EXERCISE',
    buttonLabel: '自分の運動の記録',
    image: 'MyRecommend-2',
    buttonId: 'my_exercise'
  },
  {
    title: 'MY DIARY',
    buttonLabel: '自分の日記',
    buttonId: 'my_diary',
    image: 'MyRecommend-3'
  }
]

export const EXERCISE = {
  type: '家事全般（立位・軽い）',
  value: '26',
  time: '10'
}

export const DIARY = {
  day: '2021.05.21',
  time: '23:25',
  content: '私の日記の記録が一部表示されます。',
  content1:
    'テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…'
}

export const TIME_TYPE = ['日', '週', '月', '年']

export const CHART = {
  [TIME_TYPE[0]]: [
    { name: '0 時', u1: 600, u2: 620 },
    { name: '2 時', u1: 500, u2: 550 },
    { name: '4 時', u1: 400, u2: 350 },
    { name: '6 時', u1: 300, u2: 450 },
    { name: '8 時', u1: 340, u2: 390 },
    { name: '10 時', u1: 390, u2: 420 },
    { name: '12 時', u1: 310, u2: 350 },
    { name: '14 時', u1: 250, u2: 290 },
    { name: '16 時', u1: 200, u2: 210 },
    { name: '18 時', u1: 150, u2: 250 },
    { name: '20 時', u1: 100, u2: 200 },
    { name: '22 時', u1: 50, u2: 350 }
  ],
  [TIME_TYPE[1]]: [
    { name: '月', u1: 600, u2: 620 },
    { name: '火', u1: 500, u2: 550 },
    { name: '水', u1: 400, u2: 350 },
    { name: '木', u1: 300, u2: 450 },
    { name: '金', u1: 340, u2: 390 },
    { name: '土', u1: 390, u2: 420 },
    { name: '日', u1: 310, u2: 350 }
  ],
  [TIME_TYPE[2]]: [
    { name: '- 日', u1: 600, u2: 620 },
    { name: '三日', u1: 500, u2: 550 },
    { name: '六 日', u1: 400, u2: 350 },
    { name: '九 日', u1: 300, u2: 450 },
    { name: '十二日', u1: 340, u2: 390 },
    { name: '十五日', u1: 390, u2: 420 },
    { name: '十八日', u1: 310, u2: 350 },
    { name: '二十一', u1: 250, u2: 290 },
    { name: '二十三', u1: 200, u2: 210 },
    { name: '二十六', u1: 150, u2: 250 },
    { name: '二十九', u1: 50, u2: 50 }
  ],
  [TIME_TYPE[3]]: [
    { name: '6 月', u1: 600, u2: 600 },
    { name: '7 月', u1: 500, u2: 550 },
    { name: '8 月', u1: 400, u2: 350 },
    { name: '9 月', u1: 300, u2: 450 },
    { name: '10 月', u1: 340, u2: 390 },
    { name: '11 月', u1: 390, u2: 420 },
    { name: '12 月', u1: 310, u2: 350 },
    { name: '1 月', u1: 250, u2: 290 },
    { name: '2 月', u1: 200, u2: 210 },
    { name: '3 月', u1: 150, u2: 250 },
    { name: '4 月', u1: 100, u2: 200 },
    { name: '5 月', u1: 50, u2: 350 }
  ]
}
