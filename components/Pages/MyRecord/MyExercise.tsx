import React from 'react'
import { Inter } from '@next/font/google'
import clsx from 'clsx'
import { EXERCISE } from './Data'

const inter = Inter({ subsets: ['latin'] })

export default function MyDiary() {
  return (
    <div
      className="container mx-auto mb-[56px] bg-dark-500 px-6 py-4 text-white"
      id="my_exercise"
    >
      <div
        className={clsx(inter.className, 'mb-2 flex items-center font-normal')}
      >
        <div className="mr-4 text-[15px]">
          MY <br /> EXERCISE
        </div>
        <div className="text-[22px]">2021.05.21</div>
      </div>
      <div className="custom-scrollbar mb-4 max-h-[230px] overflow-y-scroll pr-6">
        <div className="grid grid-cols-1  gap-y-2 lg:grid-cols-2 lg:gap-x-[40px]">
          {Array.from(Array(30).keys()).map(() => (
            // eslint-disable-next-line react/jsx-key
            <div className="flex border-b border-gray-400">
              <div className="mr-2 mt-2 h-[5px] w-[5px] rounded-full bg-white"></div>
              <div className="flex-1">
                <div className="flex justify-between">
                  <div className="text-[15px]">{EXERCISE.type} </div>
                  <div
                    className={clsx(
                      inter.className,
                      'mr-2 text-lg font-normal text-primary-300'
                    )}
                  >
                    {EXERCISE.time} min
                  </div>
                </div>
                <div
                  className={clsx(
                    inter.className,
                    'text-[15px] font-normal text-primary-300'
                  )}
                >
                  {EXERCISE.value}kcal
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}
