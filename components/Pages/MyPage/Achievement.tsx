import React from 'react'
import mainPhoto from '@/assets/images/main_photo.jpg'
import missCircle from '@/assets/images/miss_circle.png'
import { Inter } from '@next/font/google'
import clsx from 'clsx'

const inter = Inter({ subsets: ['latin'] })

export default function Achievement() {
  return (
    <div
      className="flex min-h-[300px] items-center justify-center bg-cover bg-no-repeat"
      style={{ backgroundImage: `url(${mainPhoto.src})` }}
    >
      <div
        className={clsx(
          inter.className,
          'flex h-[181px] w-[181px] items-center justify-center bg-cover font-normal text-white'
        )}
        style={{ backgroundImage: `url(${missCircle.src})` }}
      >
        <span className="mr-1 text-xl drop-shadow-[0px_0px_6px_#FC7400]">
          05/21
        </span>
        <span className="text-[25px] drop-shadow-[0px_0px_6px_#FCA500]">
          75%
        </span>
      </div>
    </div>
  )
}
