import React from 'react'
import { Inter } from '@next/font/google'
import clsx from 'clsx'
import Image from 'next/image'
import bgImage from '@/assets/images/bg.png'
import { CATEGORY } from './Data'

const inter = Inter({ subsets: ['latin'] })

export default function ListCategory() {
  return (
    <div className="container mx-auto mb-6">
      <div className="flex flex-row flex-wrap justify-around xl:px-[70px]">
        {CATEGORY.map((item) => (
          <div
            className="flex h-[136px] w-[116px]	cursor-pointer flex-col items-center justify-center bg-scroll bg-center bg-no-repeat"
            key={item.title}
            style={{ backgroundImage: `url(${bgImage.src})` }}
          >
            <Image
              alt={item.title}
              width={56}
              src={require(`@/assets/icons/${item.image}.png`)}
            />
            <div
              className={clsx(
                inter.className,
                'text-center text-[20px] font-normal text-white'
              )}
            >
              {item.title}
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}
