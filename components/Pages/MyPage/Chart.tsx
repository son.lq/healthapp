import React from 'react'
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  ResponsiveContainer
} from 'recharts'
import { CHART } from './Data'

export default function Chart() {
  return (
    <div className="bg-dark-600 pt-2 text-[8px] xl:pl-[60px]">
      <ResponsiveContainer height={292} width="100%">
        <LineChart data={CHART}>
          <Line type="monotone" dataKey="u1" stroke="#8FE9D0" strokeWidth={3} />
          <Line type="monotone" dataKey="u2" stroke="#FFCC21" strokeWidth={3} />
          <CartesianGrid horizontal={false} stroke="#777777" />
          <XAxis
            dataKey="name"
            stroke="#fff"
            axisLine={false}
            tickLine={false}
          />
          <YAxis width={15} tickCount={0} />
        </LineChart>
      </ResponsiveContainer>
    </div>
  )
}
