export const CHART = [
  { name: '6 月', u1: 600, u2: 620 },
  { name: '7 月', u1: 500, u2: 550 },
  { name: '8 月', u1: 400, u2: 350 },
  { name: '9 月', u1: 300, u2: 450 },
  { name: '10 月', u1: 340, u2: 390 },
  { name: '11 月', u1: 390, u2: 420 },
  { name: '12 月', u1: 310, u2: 350 },
  { name: '1 月', u1: 250, u2: 290 },
  { name: '2 月', u1: 200, u2: 210 },
  { name: '3 月', u1: 150, u2: 250 },
  { name: '4 月', u1: 100, u2: 200 },
  { name: '5 月', u1: 50, u2: 350 }
]

export const CATEGORY = [
  { title: 'Morning', image: 'icon_knife' },
  { title: 'Lunch', image: 'icon_knife' },
  { title: 'Dinner', image: 'icon_knife' },
  { title: 'Snack', image: 'icon_cup' }
]

export const LIST = [
  {
    image: 'm01',
    time: '05.21.Morning'
  },
  {
    image: 'l03',
    time: '05.21.Lunch'
  },
  {
    image: 'd01',
    time: '05.21.Dinner'
  },
  {
    image: 'l01',
    time: '05.21.Snack'
  },
  {
    image: 'm01',
    time: '05.20.Morning'
  },
  {
    image: 'l02',
    time: '05.20.Lunch'
  },
  {
    image: 'd02',
    time: '05.20.Dinner'
  },
  {
    image: 's01',
    time: '05.21.Snack'
  }
]
