import React from 'react'
import { Inter } from '@next/font/google'
import clsx from 'clsx'
import Image from 'next/image'
import { nanoid } from 'nanoid'
import { LIST } from './Data'

const inter = Inter({ subsets: ['latin'] })

export default function List() {
  return (
    <div className="container mx-auto">
      <div className="mb-[28px] grid grid-cols-2 gap-2 lg:grid-cols-4">
        {LIST.map((item) => (
          <div className="flex flex-col" key={nanoid()}>
            <div className="relative aspect-square overflow-hidden">
              <Image
                className="h-full object-cover"
                alt={item.image}
                src={require(`@/assets/images/${item.image}.jpg`)}
              />

              <div
                className={clsx(
                  inter.className,
                  'absolute bottom-0 left-0 bg-primary-300 py-1 pl-1 pr-5 text-[15px] font-normal text-white'
                )}
              >
                {item.time}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}
