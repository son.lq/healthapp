import React from 'react'
import { Inter } from '@next/font/google'
import clsx from 'clsx'
import { RECOMMENDED } from './Data'

const inter = Inter({ subsets: ['latin'] })

export default function Recommended() {
  return (
    <div className="container mx-auto mb-14">
      <div className="grid grid-cols-1 gap-8 lg:grid-cols-4">
        {RECOMMENDED.map((item) => (
          <div
            className="flex flex-col items-center bg-dark-600 py-6"
            key={item.subTitle}
          >
            <div
              className={clsx(
                inter.className,
                'mb-[10px] text-center text-[22px] font-normal text-primary-300'
              )}
            >
              {item.title}
            </div>
            <div className="mb-[10px] h-[1px] w-[56px] bg-white"></div>
            <div className="text-lg text-white">{item.subTitle}</div>
          </div>
        ))}
      </div>
    </div>
  )
}
