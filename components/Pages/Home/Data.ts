export const LIST = [
  'column-1',
  'column-2',
  'column-3',
  'column-4',
  'column-5',
  'column-6',
  'column-7',
  'column-8'
]

export const RECOMMENDED = [
  { title: 'RECOMMENDED COLUMN', subTitle: 'オススメ' },
  { title: 'RECOMMENDED DIET', subTitle: 'ダイエット' },
  { title: 'RECOMMENDED BEAUTY', subTitle: '美容' },
  { title: 'RECOMMENDED HEALTH', subTitle: '健康' }
]
