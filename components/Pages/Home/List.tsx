import React from 'react'
import { Inter } from '@next/font/google'
import clsx from 'clsx'
import Image from 'next/image'
import { LIST } from './Data'
const inter = Inter({ subsets: ['latin'] })

export default function List() {
  return (
    <div className="container mx-auto">
      <div className="mb-[26px] grid grid-cols-2 gap-x-2 gap-y-[18px] lg:grid-cols-4">
        {LIST.map((item) => (
          <div className="flex flex-col" key={item}>
            <div className="relative mb-2 max-h-[144px] overflow-hidden">
              <Image
                className="h-full object-cover"
                alt={item}
                src={require(`@/assets/images/${item}.jpg`)}
              />

              <div
                className={clsx(
                  inter.className,
                  'absolute bottom-0 left-0 bg-primary-300 px-2 text-[15px] font-normal text-white'
                )}
              >
                2021.05.17 23:25
              </div>
            </div>
            <div className="mb-2 text-[15px] line-clamp-2">
              魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ
            </div>
            <div className="text-xs text-primary-400">#魚料理 #和食 #DHA</div>
          </div>
        ))}
      </div>
    </div>
  )
}
