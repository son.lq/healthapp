import React, { useState, useRef, useEffect } from 'react'
import Image from 'next/image'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { useLoginContext } from '@/context/Login'
import ToolBar from './ToolBar'
import SidebarLink from './SidebarLink'
import { LIST_MENU_SHOW } from './Data'

export default function Header() {
  const [showMenu, setShowMenu] = useState(false)
  const [isFirstCome, setIsFirstCome] = useState(true)
  const wrapperRef = useRef(null)
  const ButtonRef = useRef(null)
  const router = useRouter()
  const { isLogin, setIsLogin } = useLoginContext()

  useEffect(() => {
    window && window.addEventListener('mousedown', handleClickOutside)
    return () => {
      window && window.removeEventListener('mousedown', handleClickOutside)
    }
  })

  useEffect(() => {
    if (isFirstCome) return setIsFirstCome(false)
    if (isLogin) {
      router.push('/my-page')
    } else {
      router.push('/')
    }
  }, [isLogin])

  const handleShowMenu = () => {
    setShowMenu(!showMenu)
  }

  const handleClickOutside = (e: { target: any }) => {
    if (
      showMenu === true &&
      wrapperRef.current &&
      ButtonRef.current &&
      !wrapperRef.current.contains(e?.target) &&
      !ButtonRef.current.contains(e?.target)
    ) {
      setShowMenu(false)
    }
  }

  return (
    <div className="bg-dark-600 shadow-sm">
      <div className="container mx-auto flex flex-wrap text-white">
        <div className="flex flex-1 justify-center lg:justify-start">
          <Link href="/" passHref>
            <Image
              src={require('@/assets/images/logo.png')}
              alt="logo"
              width={144}
            />
          </Link>
        </div>
        <div className="mr-[35px] flex py-4">
          {LIST_MENU_SHOW.map((item) => (
            <SidebarLink item={item} key={item.name} />
          ))}
        </div>
        <div className="relative flex flex-1 items-center justify-center hover:cursor-pointer md:flex-none md:justify-end">
          <button className="mr-3" onClick={setIsLogin}>
            {isLogin ? 'ログアウト' : 'ログイン'}
          </button>
          <button
            ref={ButtonRef}
            className="hover:bg-primary-400/[.15] focus:outline-none"
            onClick={handleShowMenu}
          >
            <Image
              src={require(`@/assets/icons/${
                showMenu ? 'icon_close.png' : 'icon_menu.png'
              }`)}
              alt="memu"
              width={32}
              height={32}
            />
          </button>
          <ToolBar wrapperRef={wrapperRef} show={showMenu} />
        </div>
      </div>
    </div>
  )
}
