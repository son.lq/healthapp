import { useMemo } from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'
import Image from 'next/image'
import clsx from 'clsx'
import { Inter } from '@next/font/google'

const inter = Inter({ subsets: ['latin'] })
type Props = {
  item: {
    name: string
    image: string
    isNotification?: boolean | undefined
    url?: string | undefined
  }
}
export default function SidebarLink({ item }: Props) {
  const router = useRouter()
  const isActive = useMemo(() => {
    return (
      router.pathname === item.url ||
      (router.asPath.indexOf(item.url) === 0 && item.url !== '/')
    )
  }, [router, item.url])

  return (
    <Link
      href={item.url || '#'}
      className={clsx(
        isActive && 'text-primary-400',
        'mr-3 flex items-center hover:text-primary-400 md:mr-6'
      )}
      key={item.name}
    >
      <div className="relative">
        <Image
          className="mr-2"
          src={require(`@/assets/icons/${item.image}.png`)}
          alt={item.name}
          width={32}
          height={32}
        />
        {item.isNotification && (
          <div
            className={clsx(
              inter.className,
              'absolute top-0 right-0 flex h-4 w-4 items-end justify-center rounded-full bg-primary-500 text-[10px] text-white'
            )}
          >
            1
          </div>
        )}
      </div>
      <span className="md:min-w-[96px]">{item.name}</span>
    </Link>
  )
}
