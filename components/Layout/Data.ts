export const FOOTER = [
  '会員登録',
  '運営会社',
  '利用規約',
  '個人情報の取扱について',
  '特定商取引法に基づく表記',
  'お問い合わせ'
]

export const LIST_MENU_SHOW = [
  { name: '自分の記録', image: 'icon_memo', url: '/record' },
  { name: 'チャレンジ', image: 'icon_challenge' },
  { name: 'お知らせ', image: 'icon_info', isNotification: true }
]

export const LIST_MENU_DROPDOWN = [
  '自分の記録',
  '体重グラフ',
  '目標',
  '選択中のコース',
  'コラム一覧',
  '設定'
]
