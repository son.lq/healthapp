import React from 'react'
import { FOOTER } from './Data'

export default function Footer() {
  return (
    <div className="bg-dark-600">
      <div className="container mx-auto flex flex-wrap py-[56px] text-[11px] text-white">
        {FOOTER.map((item) => (
          <div className="mr-[45px] cursor-pointer" key={item}>
            {item}
          </div>
        ))}
      </div>
    </div>
  )
}
