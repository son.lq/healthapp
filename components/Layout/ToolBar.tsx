import React from 'react'
import { LIST_MENU_DROPDOWN } from './Data'

export default function ToolBar({
  show,
  wrapperRef
}: {
  show: boolean
  wrapperRef: any
}) {
  return (
    <div
      ref={wrapperRef}
      className={`absolute top-[48px] right-0 z-[9999] flex flex-col overflow-auto bg-[#777777] text-white ${
        show ? 'h-[450px] w-[280px] ' : 'h-0'
      }`}
    >
      <ul className="flex flex-col text-lg">
        {LIST_MENU_DROPDOWN.map((item) => (
          <li key={item}>
            <div className="h-[1px] bg-white/[.15]"></div>
            <a
              href="#"
              className="block py-[23px] pl-8 hover:bg-primary-300"
              aria-current="page"
            >
              {item}
            </a>
          </li>
        ))}
      </ul>
    </div>
  )
}
