import React, { ReactNode, useRef } from 'react'
import Head from 'next/head'
import { Noto_Sans_JP } from '@next/font/google'
import clsx from 'clsx'
import Image from 'next/image'
import Header from './Header'
import Footer from './Footer'

const notoSansJp = Noto_Sans_JP({
  weight: '300',
  subsets: ['japanese']
})

const DEFAULT_TITLE = 'le son'
const DEFAULT_DESCRIPTION = 'test'

type Props = {
  children?: ReactNode
  title?: string
  url?: string
  description?: string
  image?: string
  className?: string
  keywords?: string
}

// const BETA_SUPPRESSION = 'BETA_SUPPRESSION'

const Layout = ({
  children,
  title,
  url,
  keywords,
  description,
  image,
  className = ''
}: Props) => {
  const refScrollUp = useRef(null)
  const theme = 'dark'

  const handleScrollUp = () => {
    refScrollUp.current.scrollIntoView({
      behavior: 'smooth',
      inline: 'start'
    })
  }

  return (
    <div className={clsx(notoSansJp.className, `flex w-full`)}>
      <div
        ref={refScrollUp}
        className="flex w-full flex-col bg-white text-dark-500 md:flex-row"
      >
        <Head>
          <title>{title || DEFAULT_TITLE}</title>
          <meta charSet="utf-8" />
          <link rel="shortcut icon" href="/favicon.ico" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
          <meta
            property="og:title"
            content={title || DEFAULT_TITLE}
            key="title"
          />
          <meta
            name="description"
            content={description || DEFAULT_DESCRIPTION}
            key="desc"
          />
          <meta
            name="og:description"
            content={description || DEFAULT_DESCRIPTION}
            key="description"
          />
          <meta property="og:image" content={image} key="image" />
          <meta name="og:url" content={url || 'https://google.con'} />
          <meta name="keywords" content={keywords || 'test1, test2'}></meta>
          <meta name="twitter:card" content="summary_large_image" />
          <meta name="twitter:title" content={title || DEFAULT_TITLE} />
          <meta
            name="twitter:description"
            content={description || DEFAULT_DESCRIPTION}
          />
          <meta name="twitter:image" content={image} />
          <meta name="twitter:url" content={url || 'https://google.com'} />
          <meta name="twitter:site" content="@google" />
        </Head>
        <div
          id="layoutBody"
          className={`${className} relative w-full overflow-auto`}
        >
          <Header></Header>
          {children}
          <Footer></Footer>
          <div className="fixed right-20 bottom-20 hidden xl:block">
            <button
              className="group focus:outline-none"
              onClick={handleScrollUp}
            >
              <Image
                className="group-hover:hidden"
                alt="scroll"
                width={48}
                src={require(`@/assets/icons/scroll.png`)}
              />
              <Image
                className="hidden group-hover:flex"
                alt="scroll"
                width={48}
                src={require(`@/assets/icons/scroll_hover.png`)}
              />
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Layout
