import React from 'react'

export default function Button({ text }: { text?: string }) {
  return (
    <button className="button-bg cursor:pointer h-[56px] w-[296px] rounded text-lg text-white focus:outline-none">
      {text || 'コラムをもっと見る'}
    </button>
  )
}
