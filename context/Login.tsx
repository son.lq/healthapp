import { useState, createContext, useContext } from 'react'

export const LoginContext = createContext<{
  setIsLogin: () => void
  isLogin: boolean
}>({
  setIsLogin() {},
  isLogin: false
})

export const useLoginContext = () => useContext(LoginContext)

export default function Login({ children }) {
  const [isLogin, setIsLogin] = useState(false)

  const handleLogin = () => {
    setIsLogin(!isLogin)
  }
  return (
    <LoginContext.Provider
      value={{
        isLogin,
        setIsLogin: handleLogin
      }}
    >
      {children}
    </LoginContext.Provider>
  )
}
